const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Joi = require("joi");

const UserSchema = new Schema({
  user: { type: String, required: true, minlength: 5 },
  display: { type: String, default: "No-Name Tutor", minlength: 3 },
  password: { type: String, required: true, minlength: 5 },
  // Add
  learn: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Course' }]
});

function validate(req) {
  const schema = {
    user: Joi.string()
      .min(5)
      .required(),
    display: Joi.string().min(3),
    password: Joi.string()
      .min(5)
      .required()
  };
  return Joi.validate(req, schema);
}

// export the new Schema so we could modify it using Node.js
module.exports = {
  User: mongoose.model("User", UserSchema),
  validate
};
