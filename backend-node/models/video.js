const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const VideoSchema = new Schema({
  courseId: { type: Schema.Types.ObjectId, required: true },
  filename: { type: String, required: true },
  handle: { type: String, required: true },
  mimetype: { type: String, required: true },
  originalFile: { type: Object },
  originalPath: { type: String, required: true },
  size: { type: Number },
  source: { type: String, required: true },
  status: { type: String, required: true },
  uploadId: { type: String, required: true },
  url: { type: String, required: true }
});

module.exports = mongoose.model('Video', VideoSchema);
