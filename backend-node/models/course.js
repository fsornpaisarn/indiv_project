const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// this will be our data base's data structure
const CourseSchema = new Schema({
  tutor: { type: String, min: [3] },
  display: { type: String, min: [3] },
  topic: { type: String, required: true },
  subject: { type: String, required: true },
  description: { type: String, required: true },
  descriptionProfile: { type: String, required: true },
  duration: { type: String, required: true },
  location: { type: String, required: true },
  fee: { type: Number, required: true },
  tuition: { type: Number, required: true },
  img: String
});

// export the new Schema so we could modify it using Node.js
module.exports = mongoose.model('Course', CourseSchema);
