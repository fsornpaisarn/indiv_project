const request = require("supertest");

const sendReq = async tutorName => {
  const res = await request("http://localhost:8000").get(
    "/api/get_courses?tutor=" + tutorName
  );
  return res;
};

describe("request is valid", () => {
  it("no existing tutor", async () => {
    const tutorName = "qwertyuiop";
    const res = await sendReq(tutorName);
    expect(res.statusCode).toBe(200);
    expect(res.body.courses).toEqual([])
  });
  it("valid user", async () => {
    const tutorName = "GemGem";
    const res = await sendReq(tutorName);
    expect(res.statusCode).toBe(200);
    expect(res.body.courses).toBeInstanceOf(Array);
  });
});
