const express = require("express");
const router = express.Router();
const {User} = require('../models/user');
const jwt = require('jsonwebtoken');
const config = require('config');

if (!config.get('jwtPrivateKey')) {
    console.error('FATAL ERROR: jwtPrivateKey is not defined.');
    process.exit(1); 
  }
  
router.post('/', async (req, res) => {
    const {checkoutToken,courseId,userToken} = req.body;
    const { _id } = jwt.verify(userToken, config.jwtPrivateKey);
    
    const learner = await User.findById(_id);
    learner.learn.push(courseId);
    try {
        await learner.save();
        res.send('gemsssssssssssss');
    } catch (e) {
        console.log(e);
    }
    

})

module.exports = router;