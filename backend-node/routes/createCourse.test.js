const request = require("supertest");

const sendReq = async req => {
  const res = await request("http://localhost:8000")
    .post("/api/create_course")
    .send(req);
  return res;
};

let req;
beforeEach(() => {
  req = {
    token: "mockedValue",
    topic: "mockedValue",
    subject: "mockedValue",
    description: "mockedValue",
    descriptionProfile: "mockedValue",
    duration: "mockedValue",
    location: "mockedValue",
    fee: 123,
    tuition: 456,
    img: "mockedValue"
  };
});

describe("request is invalid", () => {
//   it("token is invalid", async () => {
//     const res = await sendReq(req);
//     expect(res.statusCode).toBe(400);
//   });
//   it("feilds are invalid", async () => {
//     const { topic, ...req2 } = req;
//     const res = await sendReq(req2);
//     expect(res.statusCode).toBe(400);
//   });
});

describe("request is valid", () => {
  it("token is valid", async () => {
    req.token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzhjOGM3OWRiNzRhMzRmYmNkMGZjMDQiLCJ1c2VyIjoiR2VtR2VtIiwiZGlzcGxheSI6IkdlbXMgdGhlIHR1dG9yIiwiaWF0IjoxNTUyODM0ODM4fQ.moceazrjB2K9VFwqCplzCzAmk9Z9uHFi8RPrAEPTNok";
    const res = await sendReq(req);
    expect(res.statusCode).toBe(200);
  });
});
