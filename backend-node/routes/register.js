const express = require('express');
const router = express.Router();
const { User, validate } = require('../models/user');
const _ = require('lodash');
const bcrypt = require('bcrypt');

router.post('/', async (req, res) => {
  // validate the req
  const { error } = validate(req.body);
  if (error) res.status(400).send(error.details[0].message);
  // find if the username already exist
  let user = await User.findOne({ user: req.body.user });
  if (user) return res.status(400).send('User already registered.');

  newUser = new User(_.pick(req.body, ['user', 'display', 'password']));

  const salt = await bcrypt.genSalt(10);
  newUser.password = await bcrypt.hash(newUser.password, salt);

  try {
    await newUser.save();
  } catch (e) {
    return res.status(400).send(e.errors.user.message);
  }

  res.send(_.pick(newUser, ['_id', 'display', 'user']));
});

module.exports = router;
