const express = require('express');
const Course = require('../models/course');
const jwt = require('jsonwebtoken');
const config = require('config');

const router = express.Router();

router.post('/', async (req, res) => {
  const {
    token,
    topic,
    subject,
    description,
    descriptionProfile,
    duration,
    location,
    fee,
    tuition,
    img
  } = req.body;
  try {
    const { user, display } = jwt.verify(token, config.jwtPrivateKey);
    const newCourse = new Course({
      tutor: user,
      display,
      topic,
      subject,
      description,
      descriptionProfile,
      duration,
      location,
      fee,
      tuition,
      img
    });

    await newCourse.save();

    res.send(newCourse);
  } catch (e) {
    return res.send(e);
  }
});

module.exports = router;
