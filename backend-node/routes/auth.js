const express = require('express');
const router = express.Router();
const { User, validate } = require('../models/user');
const _ = require('lodash');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('config');

if (!config.get('jwtPrivateKey')) {
  console.error('FATAL ERROR: jwtPrivateKey is not defined.');
  process.exit(1); 
}

router.post('/', async (req, res) => {
  // validate request
  const { error } = validate(req.body);
  if (error) res.status(400).send(error.details[0].message);

  // Look if the user exists
  const curUser = await User.findOne({ user: req.body.user });
  if (!curUser) return res.status(400).send('Invalid username or password');

  // validate password
  const validPassword = await bcrypt.compare(
    req.body.password,
    curUser.password
  );
  if (!validPassword)
    return res.status(400).send('Invalid username or password');
  // should not enter private key in the source code !!! should put it in the environment variable instead
  const token = jwt.sign(
    { _id: curUser._id, user: curUser.user, display: curUser.display },
    config.get('jwtPrivateKey')
  );
  res.send({ token });
});

module.exports = router;
