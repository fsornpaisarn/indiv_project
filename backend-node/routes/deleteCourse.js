const express = require('express');
const router = express.Router();
const Course = require('../models/course');
const jwt = require('jsonwebtoken');
const config = require('config');

router.post('/', async (req, res) => {
  const { token, courseId } = req.body;

  jwt.verify(token, config.jwtPrivateKey, (err, decoded) => {
    if (err) return res.send(err);

    Course.findByIdAndDelete(courseId, (err, coures) => {
      if (err) return res.send(err);
      res.send(coures);
    });
  });
});

module.exports = router;
