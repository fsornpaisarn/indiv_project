const express = require("express");
const router = express.Router();
const Course = require("../models/course");
const { User } = require("../models/user");

router.get("/:username", async (req, res) => {
  const { username } = req.params;
  let result = {};
  const tutorCourses = await Course.find({ tutor: username });
  result.tutorCourses = tutorCourses;
  // User.find({ learner }tutorC (err, courses) => {
  //   if (err) return res.status(400).send(err);
  //   res.send({ courses });
  // });
  const learnCourses = await User.find({ user: username }).populate("learn");
  result.learnCourses = learnCourses;
  res.send(result);
});

router.get("/learn", async (req, res) => {
  console.log("learner");
});

router.get("/", (req, res) => {
  Course.find({}, (err, courses) => {
    if (err) return res.status(400).send(err);
    res.send({ courses });
  });
});

// Search by filter
router.post('/', async (req, res) => {
  const { subject, location, tuitionMax, feeMax, rating } = req.body;
  const newSubject = subject.map(element => element.value);

  let input = {};
  if (location !== "") input.location = location;
  if (subject.length !== 0) input.subject = { $in: newSubject };
  if (tuitionMax.value !== "")
    input.tuition = { $lte: Number(tuitionMax.value) };
  if (feeMax.value !== '') input.fee = { $lte: Number(feeMax.value) };
  const query = await Course.find(input);
  res.send({ courses: query });
});

module.exports = router;
