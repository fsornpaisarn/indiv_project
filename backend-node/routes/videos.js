const express = require('express');
const router = express.Router();
const Video = require('../models/video');
const _ = require('lodash');
const ObjectId = require('mongoose').Types.ObjectId;

// Get all videos
router.get('/:courseId', async (req, res) => {
  const { courseId } = req.params;
  try {
    const videos = await Video.find({ courseId: new ObjectId(courseId) });
    res.send({ videos });
  } catch (e) {
    console.log(e);
    res.send(e);
  }
});

// Upload a new video
router.post('/', (req, res) => {
  const { uploaded, courseId } = req.body;
  let newVideo;

  try {
    uploaded.forEach(async item => {
      newVideo = _.pick(item, [
        'filename',
        'handle',
        'mimetype',
        'originalFile',
        'originalPath',
        'size',
        'source',
        'status',
        'uploadId',
        'url'
      ]);
      newVideo.courseId = courseId;
      newVideo = new Video(newVideo);
      await newVideo.save();
    });
    return res.send('Save to mongoDB successful');
  } catch (e) {
    return res.send(e);
  }
});

router.delete('/', async (req, res) => {
  const { videoId } = req.query;
  try {
    await Video.findByIdAndDelete(videoId);
    res.send('Delete successful');
  } catch (e) {
    console.log(e);
    res.send(e);
  }
});

module.exports = router;
