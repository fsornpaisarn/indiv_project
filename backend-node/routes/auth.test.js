const request = require("supertest");

const sendReq = async req => {
  const res = await request("http://localhost:8000")
    .post("/api/login")
    .send(req);
  return res;
};

describe("request is invalid", () => {
  it("user feild is invalid", async () => {
    const req = {
      user: "a",
      password: "1123456789"
    };
    const res = await sendReq(req);
    expect(res.statusCode).toBe(400);
    expect(res.error.text).toEqual(
      '"user" length must be at least 5 characters long'
    );
  });

  it("password is invalid", async () => {
    const req = {
      user: "abcde",
      password: "1"
    };
    const res = await sendReq(req);
    expect(res.statusCode).toBe(400);
    expect(res.error.text).toEqual(
      '"password" length must be at least 5 characters long'
    );
  });
});

describe("request is valid", () => {
  it("no existing user", async () => {
    const req = {
      user: "abcdef",
      password: "12345678"
    };
    const res = await sendReq(req);
    expect(res.statusCode).toBe(400);
    expect(res.error.text).toEqual("Invalid username or password");
  });

  it("wrong password", async () => {
    const req = {
      user: "GemGem",
      password: "123456789"
    };
    const res = await sendReq(req)
    expect(res.statusCode).toBe(400);
    expect(res.error.text).toEqual('Invalid username or password');
  });

  it("correct credetail", async () => {
    const req = {
      user: "GemGem",
      password: "gemsgems"
    };
    const res = await sendReq(req)
    expect(res.statusCode).toBe(200);
  });
});
