const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const _ = require('lodash');
const morgan = require('morgan');

// import routes
const createCourse = require('./routes/createCourse');
const register = require('./routes/register');
const auth = require('./routes/auth');
const getCourses = require('./routes/getCourses');
const deleteCourse = require('./routes/deleteCourse');
const chargeMoney = require("./routes/chargeMoney")
const videos = require('./routes/videos');

//Middleware
const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(morgan('tiny'));

// routes
app.use('/api/create_course', createCourse);
app.use('/api/register', register);
app.use('/api/login', auth);
app.use('/api/get_courses', getCourses);
app.use('/api/delete_course', deleteCourse);
app.use('/api/charge_money', chargeMoney)
app.use('/api/videos', videos);

// choose database
const fudgy = true;
const database = fudgy
  ? 'mongodb://fudgy:fudgylor1234@ds237669.mlab.com:37669/indiv-project'
  : 'mongodb://chain:chainlor123@ds119795.mlab.com:19795/se_project';

//Database connection
mongoose.connect(database, { useNewUrlParser: true }, err => {
  if (err) {
    console.log('Some problem with the connection ' + err);
  } else {
    console.log('The Mongoose connection is ready');
  }
});

//Get all courses
app.get('/', (req, res) => {
  mongoose.connection.db.collection('courses', (err, collection) => {
    if (err) res.send(err);
    collection.find({}).toArray((err, data) => {
      res.json(data);
    });
  });
});

app.listen(process.env.PORT || 8000);
