import React, { Component } from 'react';
import ReactFilestack from 'filestack-react';
import axios from 'axios';

const options = {
  accept: ['video/*', 'text/*']
};

class CourseVideo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      videos: []
    };
  }

  componentDidMount() {
    console.log('Mounted', this.props.match.params.courseId);
    // Query all videos to display
    this.getVideos();
  }

  // Get all videos
  async getVideos() {
    const res = await axios.get(
      `http://localhost:8000/api/videos/${this.props.match.params.courseId}`
    );
    console.log('res', res);
    this.setState({ videos: res.data.videos });
  }

  onUploadSuccess = async uploaded => {
    try {
      const response = await axios.post('http://localhost:8000/api/videos', {
        uploaded: uploaded.filesUploaded,
        courseId: this.props.match.params.courseId
      });

      // Query all videos to display
      this.getVideos();
    } catch (e) {
      console.log(e);
    }
  };

  onDeleteVideo = async videoId => {
    try {
      await axios.delete(`http://localhost:8000/api/videos?videoId=${videoId}`);
      this.getVideos();
    } catch (e) {
      console.log(e);
    }
  };
  render() {
    const { videos } = this.state;
    console.log('this.state.videos', videos);
    return (
      <div className="my-4">
        <div className="row">
          <div className="col-sm-10">
            {videos && videos.length > 0 ? (
              <div>
                <h4>
                  Video <span className="text-orange">List</span>
                </h4>

                <ul className="list-group mt-3">
                  {videos.map(video => (
                    <li key={video._id} className="list-group-item">
                      <a href={video.url}>{video.filename}</a>
                      <button
                        className="btn btn-danger float-right"
                        onClick={() => this.onDeleteVideo(video._id)}
                      >
                        Delete
                      </button>
                    </li>
                  ))}
                </ul>
              </div>
            ) : (
              <h3>No Videos</h3>
            )}
          </div>
          <div className="col-sm-2">
            <ReactFilestack
              apikey={process.env.REACT_APP_FILESTACK_API_KEY}
              buttonText="Upload a video"
              buttonClass="btn btn-primary btn-lg btn-block"
              options={options}
              onSuccess={this.onUploadSuccess}
              preload={true}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default CourseVideo;
