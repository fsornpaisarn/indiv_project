import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../courseListing/CourseContainer.css';

//query
import axios from 'axios';
import querystring from 'query-string';

//redux
import { EditCourseAction } from '../../actions/EditCourseAction';
import { connect } from 'react-redux';
const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
  setEditCourse: course => dispatch(EditCourseAction(course))
});

class MyCoursesContainer extends Component {
  onDeleteCourse = async event => {
    await axios.post('http://localhost:8000/api/delete_course', {
      token: window.localStorage.getItem('token'),
      courseId: this.props.info._id
    });
    window.location.reload();
  };

  onEditCourse = event => {
    this.props.setEditCourse(this.props.info);
  };

  render() {
    const {
      topic,
      description,
      descriptionProfile,
      subject,
      duration,
      location,
      tuition,
      display,
      fee,
      img,
      _id
    } = this.props.info;

    const index = this.props.index;
    return (
      <div className="col-sm-6 col-md-4 col-lg-3 px-4">
        <div
          className="card shadow mb-5"
          data-toggle="modal"
          data-target={`#modal${index}`.toLowerCase()}
        >
          <div className="modal" id={`modal${index}`.toLowerCase()}>
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title pt-3 pb-3"> {topic} </h5>
                  <button className="close">&times;</button>
                </div>

                <div className="modal-body">
                  <p className="modal-text"> {description} </p>
                  <hr />
                  <strong className="modal-text">Instructor: </strong>
                  <a className="modal-text" href="#">
                    {' '}
                    {display}{' '}
                  </a>
                  <p className="modal-text"> {descriptionProfile} </p>
                  <hr />
                  <strong className="modal-text">location: </strong>
                  <span className="modal-text"> {location}</span>
                  <br />
                  <strong className="modal-text">Duration:</strong>
                  <span className="modal-text"> {duration} </span>
                  <br />
                  <strong className="modal-text">Fee: </strong>
                  <span className="modal-text">฿{fee}</span>
                </div>

                <div className="modal-footer" id="abc">
                  <Link to={`/courseVideo/${_id}`} className="mr-auto">
                    Videos
                  </Link>
                  <button
                    className="btn btn-orange"
                    onClick={this.onEditCourse}
                  >
                    <Link to="/create_course" id="edit">
                      Edit
                    </Link>
                  </button>
                  <button
                    className="btn btn-secondary"
                    onClick={this.onDeleteCourse}
                    id="delete"
                  >
                    Delete
                  </button>
                </div>
              </div>
            </div>
          </div>

          <img
            className="card-img-top img-fluid"
            src={img}
            alt="unable to load file"
          />

          <div className="card-body">
            <h5 className="card-title">{topic}</h5>
            <hr />
            <strong className="card-text">Instructor: </strong>
            <span className="card-text"> {display} </span>
            <hr />
            <strong className="card-text limitP">Score: </strong>
            <i className="star icon" />
            <i className="half star icon" />
            <br />
          </div>

          <div className="card-footer d-flex">
            <h5>{subject}</h5>
            <h5 className="ml-auto">฿{tuition}</h5>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyCoursesContainer);
