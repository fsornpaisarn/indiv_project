import React, { Component } from 'react';
import CowBg from '../CowBg';
import './myCourses.css';
import '../courseListing/CourseContainer.css';
import MyCoursesContainer from './MyCoursesContainer';
import books from '../../img/books.svg';
import teacher from '../../img/teacher.svg';
import Loader from '../loader/Loader';
import axios from 'axios';

export default class MyCourses extends Component {
  constructor() {
    super();
    this.state = {
      coursesAsTutor: [],
      coursesAsStudent: [],
      isLoading: false
    };
  }

  componentDidMount() {
    this.setState({ isLoading: true });
    this.fetchTutorCourses();
    // this.fetchStudentCourses();
  }

  async fetchTutorCourses() {
    try {
      const response = await axios.get(
        `http://localhost:8000/api/get_courses/${
          window.localStorage.username
        }`
      );
      console.log(response.data)
      this.setState({
        coursesAsTutor: response.data.tutorCourses,
        coursesAsStudent: response.data.learnCourses[0].learn,
        isLoading: false
      });
    } catch (e) {
      console.log(e);
    }
  }

  // async fetchStudentCourses() {
  //   try {
  //     console.log("fetching")
  //     const response = await axios.get(
  //       `http://localhost:8000/api/get_courses/learn?learner=${
  //         window.localStorage.username
  //       }`
  //     );
  //     console.log("fetched")
  //     this.setState({
  //       coursesAsStudent: response.data.courses,
  //       isLoading: false
  //     });
  //     console.log("setted")
  //   } catch (error) {
  //     console.log(error);
  //   }
  // }

  render() {
    const { coursesAsTutor, isLoading,coursesAsStudent } = this.state;
    return (
      <div>
        <CowBg />
        <div className="topic">
          <h1 className="display-4">My Courses</h1>
          <hr />
        </div>

        <span className="display-4 topic" style={{ fontSize: '30px' }}>
          {' '}
          As a Tutor
        </span>
        <img src={teacher} style={{ width: '40px', height: 'auto' }} />
        <br />
        <br />
        <div className="row">
          {coursesAsTutor.map((course, index) => (
            <MyCoursesContainer key={course._id} info={course} index={index} />
          ))}
        </div>

        

        <br />
        <br />
        <span className="display-4 topic" style={{ fontSize: '30px' }}>
          {' '}
          As a Learner
        </span>
        <img src={books} style={{ width: '40px', height: 'auto' }} />
        <br />
        <br />
        <div className="row">
          {coursesAsStudent.map((course, index) => (
            <MyCoursesContainer key={index} info={course} index={index} />
          ))}
        </div>
        {isLoading ? Loader : null}
      </div>
    );
  }
}
