const subjects = [
  {
    value: 'GAT-ENG',
    label: 'GAT-ENG'
  },
  {
    value: 'English',
    label: 'ENG'
  },
  {
    value: 'Mathematics',
    label: 'Mathemetics'
  },
  {
    value: 'Biology',
    label: 'Biology'
  },
  {
    value: 'Pat1',
    label: 'PAT1'
  },
  {
    value: 'Pat2',
    label: 'PAT2'
  },
  {
    value: 'Pat3',
    label: 'PAT3'
  },
  {
    value: 'CU-TEP',
    label: 'CU-TEP'
  },
  {
    value: 'TU GET',
    label: 'TU-GET'
  }
];

const tuitionFees = [
  {
    value: '1000000000',
    label: '-none-'
  },
  {
    value: '200',
    label: '< 200 Baht'
  },
  {
    value: '300',
    label: '< 300 Baht'
  },
  {
    value: '500',
    label: '< 500 Baht'
  },
  {
    value: '1000',
    label: '< 1000 Baht'
  }
];

const joiningFees = [
  {
    value: '1000000000',
    label: '-none-'
  },
  {
    value: '200',
    label: '< 200 Baht'
  },
  {
    value: '300',
    label: '< 300 Baht'
  },
  {
    value: '500',
    label: '< 500 Baht'
  },
  {
    value: '1000',
    label: '< 1000 Baht'
  }
];

const ratings = [
  {
    value: '',
    label: '-none-'
  },
  {
    value: '2',
    label: '2'
  },
  {
    value: '3',
    label: '3'
  },
  {
    value: '5',
    label: '5'
  }
];

export { subjects, tuitionFees, joiningFees, ratings };
