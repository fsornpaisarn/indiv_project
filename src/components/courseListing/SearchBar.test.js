import settedUp from "../../setupTests";
import { shallow } from "enzyme";
import React from "react";
import SearchBar from "./SearchBar";

let searchKey;
let mockOnChange;
let mockOnSearch;

//Set precondition before each "it"
beforeEach(() => {
  searchKey = "";
  mockOnChange = jest.fn(e => {
    searchKey = e;
  });
  mockOnSearch = jest.fn();
});

describe("display testing", () => {
  it("display SearchBar correctly", () => {
    expect(
      shallow(
        <SearchBar
          onChange={mockOnChange}
          onSearch={mockOnSearch}
          searchValue={searchKey}
        />
      )
    ).toMatchSnapshot();
  });
});

describe("test on typing input", () => {
  it("searchKey changed", () => {
    let searchKey = "";
    let newInput = "new input";
    let mockOnChange = jest.fn(e => {
      searchKey = e;
    });
    let wrapper = shallow(
      <SearchBar
        onChange={mockOnChange}
        onSearch={mockOnSearch}
        searchValue={searchKey}
      />
    );
    wrapper.find("[id='search field']").simulate("change", newInput);
    expect(mockOnChange.mock.calls.length).toBe(1);
    expect(searchKey).toBe(newInput);
  });

  it("retype input", () => {
    let searchKey = "";
    let firstInput = "first input";
    let secondInput = "second input";
    let mockOnChange = jest.fn(e => {
      searchKey = e;
    });
    let wrapper = shallow(
      <SearchBar
        onChange={mockOnChange}
        onSearch={mockOnSearch}
        searchValue={searchKey}
      />
    );
    wrapper.find("[id='search field']").simulate("change", firstInput);
    wrapper.find("[id='search field']").simulate("change", secondInput);
    expect(mockOnChange.mock.calls.length).toBe(2);
    expect(searchKey).toBe(secondInput);
  });
});
