import React from 'react';

import './CourseContainer.css';

export default props => (
  <div className="row">
    <div className="col">
      <form onSubmit={props.onSearch}>
        <div className="input-group" id="search form">
          <input
            type="text"
            className="form-control search-form"
            placeholder="Search Tutor"
            onChange={e => props.onChange(e)}
            value={props.searchValue}
            id = "search field"
          />
          <div className="input-group-append">
            <input
              type="submit"
              className="btn btn-dark search-btn"
              value="Search"
              id = "search submit"
            />
          </div>
        </div>
      </form>
    </div>
  </div>
);
