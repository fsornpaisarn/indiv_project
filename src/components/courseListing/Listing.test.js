import settedUp from "../../setupTests";
import { shallow } from "enzyme";
import React from "react";
import axios from "axios";

import Listing from "./Listing";
import Loader from "../loader/Loader";
import CourseContainer from "./CourseContainer";

//Set precondition before each "it"
let wrapper;
let mockEvent;
beforeEach(() => {
  wrapper = shallow(<Listing />);
  mockEvent = {
    target: {
      value: "mocked value"
    },
    preventDefault: function() {}
  };
});

describe("display Listing class testing", () => {
  it("display Listing correctly", () => {
    expect(wrapper).toMatchSnapshot();
  });
});

describe("Fetching data", () => {
  it("state testing", async () => {
    expect.assertions(2);
    expect(wrapper.state("isLoading")).toBe(true);
    await wrapper.instance().componentDidMount();
    await expect(wrapper.state("isLoading")).toBe(false);
  });
});

describe("state changing test", () => {
  it("onChange method", () => {
    wrapper.instance().onChange(mockEvent);
    expect(wrapper.state("location")).toBe(mockEvent.target.value);
  });
  it("onSearchChange method", () => {
    wrapper.instance().onSearchChange(mockEvent);
    expect(wrapper.state("search")).toBe(mockEvent.target.value);
  });
  it("onSearch method", async () => {
    expect.assertions(2);
    let mockSearch = "gems";
    wrapper.setState({ search: mockSearch });

    await wrapper.instance().onSearch(mockEvent);
    const response = await axios({
      method: "GET",
      url: `http://localhost:8000/api/get_courses?tutor=${mockSearch}`
    });

    expect(wrapper.state("courseList")).toEqual(response.data.courses);
    expect(wrapper.state("isLoading")).toBe(false);
  });
});

describe("display Loader correctly", () => {
  it("isn't loading", () => {
    wrapper.setState({ isLoading: false });
    expect(wrapper.contains(Loader)).toBe(false);
  });
  it("is loading", () => {
    wrapper.setState({ isLoading: true });
    expect(wrapper.contains(Loader)).toBe(true);
  });
});

describe("display courses correctly", () => {
  it("no course to display", () => {
    let mockCourses = [];
    wrapper.setState({ courseList: mockCourses });
    expect(wrapper.find(CourseContainer).length).toBe(mockCourses.length);
  });

  it("display courses correctly", () => {
    let mockCourses = [
      {
        topic: "Node with React: Fullstack Web Development",
        description:
          "Build and deploy fullstack web apps with NodeJS, React, Redux, Express, and MongoDB.",
        descriptionProfile:
          "Stephen Grider\nStephen Grider has been building complex Javascript front ends for top corporations in the San Francisco Bay Area.",
        subject: "English",
        duration: "Every Tuesday 5pm",
        location: "Udemy",
        tuition: 1300,
        tutor_display: "chain",
        fee: 150,
        img:
          "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCACHAPADASIAAhEBAxEB/8QAHAABAAIDAQEBAAAAAAAAAAAAAAQFAgYHAwEI/8QAThAAAQQCAAQCBgQJBwcNAAAAAQACAwQFEQYSITETQQcUFSJRYTJTcdEWI0JUgZGUldQkRZKTobHSCBczNFLB0xglQ0RVVnR1oqSytPH/xAAcAQEAAQUBAQAAAAAAAAAAAAAAAgEDBAUHBgj/xAA2EQABAwIEAwUGBQUBAAAAAAABAAIRAwQFEiExQVFhBhOBkdEUIjJxsfAWUqHB8QcVIzRCcv/aAAwDAQACEQMRAD8A7miIu+r5SRERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERERF70sfZyM3hVYXzya2Q0dh8T8F9vY61jJRHagfA8jYDh3HyPmo525ss68lc7t+TvMpy8+Hmo6IikraIoOIt27tUyXKZpSh5AjJ3sdOv94+etjoQpyo05hIUnNLSQUREVVFEREREWEsnhRPeRsNaXaHnobUfF3xlKMVpsM1cSAnwrDOSRvXXvDyPyVJEwpZTGbgpaIiqooiIiIiIiIiIiIiIiIiIiIiIiIiIiIthxPBVzK022fFjrseNsD9kuHx6dgtYnqtsOY5z5WlocAI5HNB2NHYB6/L4eSyjbbhjbGzM5lrGgNa0ZObQA7D6SxLgXBEUIB6/wthaG0a4m6BI5D+Quq8LYJ2BoyRyuY+aV/M4s7Aa0Bvz8/1rPiXBnPUGQMeyKRkge17wTrodjp8VzXHZjJ4zxPCyt+Xn1v1qy6bWvhzk6/Qpn4W5j8/l/UPuWm9gujV74uGbx9F6T+7WAoezBjskRw9VEyuKsYe46vYADwNhzTsOHxCiKTfydrJvY+3OZnMGg5+hoKisuu4enPl7Ugno2YjNBWGg6DURkY0nvuSMF/XsQQts+5bb92y4IzO00+/slefp2T7vvalo0ljBOu8Ex99ASrRFqXAXHw45bbIoOo+A2N+nSh/MH83wA19H+1X8Ocx89p1aO7C6w3mBj5+o0dH9SuW13RvKLa9B0sOx+Rjj1UcQw27wu6fZXlMsqMiRoYkAjaRsQV9eL/taMtMPs7wjzg78XxPLXly67+e/0qavP1iL62P+mE9Zh+tj/pj71kCBxWAZMaKHm7N2rRMmPrCzPzAch6+75nWxvyHfpvfXWjOjLjG0vaGvIHM1p2AfMA+f2rD1mH62P+mPvUb25j/XRT9chNouDREHbdsjYH26VNjMqupaBlU5EV/w/wAHzZ2u6w6YV6+y1ruXmLj56HwUatVlFueoYCnQt6ty/u6QkqLR4WyeRrCxBW3EerS5wbzfYCqyWJ8Er4pGGORh5XNcNEFdmiiEMTI2jTWNDRofAaWu57gh2byXrbJzC17QHjwi7qBoELR0MVDqhFWA3huvU3WAllJptyXO47foucqPfknipTvqxCawGExxuOg53kth4i4Xs8PPjMh8WCTYZKGlvUeRHxVN279FvGPbWZmYdCvK1KT7epkqtgjgouMms2KEMluIQWHDbmDy69OnXRI0dbOt62dbUpEVwCBCtEySYRERVVEREREREREREREVdbo3JstUsRXXQ1YgRJAAff8A9x30HXtrp1JViii5ocIKk1xYZCIvCrdr3mOdXmZO1riwlh2AR5LH2nTFmWubUAniDTJGZAHM322N9N6Vcw3lMrpIjZSUWEc0cpIZIx5HU8rgf7lmq7qMQo+RDHY+0JGl0fgv5mtOiRynYB8uiy49nzsvo8gdk348+PGw2nVK3JJM9taTw3F2/wAl2tgAb3+g+F6Z1iK5UrVrd60IHF0VKrJO5gc0hvNyNPLsg63reirLjx9nL8DwUamIzM1prGAxDE2QQRC9vnHruQFx7tne3VPF8Kp2hlhqHvIEwJZ8Rg5RE66c+C7J2KtA7C8SfWaQSwZNxJh/w8+A0lce9AX+jyhH1Nbz39Yt4x/DVeDP25nSOnDRztila0tBkLifLy10/wB61r0P8K8QcNMyAyfDecqmSKBrN4uc7LeffZp7bC32GG+2/aecLmwxzYw0+yrOiRzb/I+Y8v1r1PZm4o0MIoUqrw0+9IJg/E46g+G6x/6hWlxd9p7yvb0y9pyQ5okaU2AwQCOY06r09n1R/wBVg/qm/d8gns6p+awf1Tfu+QWpcRZ3iXKTmpwliL9sRvdBattx0zvVpgR7h23QIHfYPdWvCV3ia5WnGc4Yy9KdjwI+TFWCHt11J90je9/ctw3GrB10bTNqOMe78g7b76GPKv7LYszDm4llBaTGUO/yDWJLPiAOsabCTAIJt3UKbWuc6tXDQNkmJugP1faqBuFoz8QVb1S0HxySuldHAWGPnYweY/WVH4mocQ5bI+HWwfETKjWGPpjZ2NcT0J+j1BB0eYdNdO6jXOFOI+HcjG2vjstcihe2bxcdj7L4jsAPAD4m7doaII18Fs/a7SPdrMBPCeHHz4Dj4LSDDcQ/6t3kc8p0J266cTwjqt5UqDLXqsQihuTxRjsxjyAP0KupXIshUhswnmimYHtPyPl9o7faFlZgbarywvLgyRhY4tOjojR0fJZRDXt2laxrn0naEgqz9vZP/tCz/WlVNuky9ZksTy2ZJpDzOd63MNn7A8BYY2lDi6cVSJ5LY965yN9ST2GgO/YAAeSlq33NJw95g8gr3tNZhOSofMhQmYisyVkmpnuYSW+JZleASNdnOI7FSK9eOpAyGJvLGwcrW7J0PtPVeqK42mxmjRCsvq1Kpmo4n5mUREU1bREREREREREREREREREREUenQr49j2VoWQtc7mIYO5VdfxuBq3HW7tagyzce2My2GM3K4A8o97udf2Dr2Vyol/FU8p4HrlWKz4Enix+KwO5Ha1sb+RI+atvZLYAHirzKhDpc467xuqLGR5KK7kPVY8A2Iy6g8DYeYQBy83IO4O+/6Oiv6ZtnxPWxXHve54Bcenz5vP7F9r42rScXwVIa7iNF0cLWEj4bACkKFOmWDUqVWqKh0C2f0VQeu3blN9uenXs5xjZ317Tqxc1mLfIA6RvVreZjSfkFs12hk24+U1L2NluDG+LGJuNrLWOueLrwzp50zw/e5hv3unZal6Nq0dyXI15QTFLlpI3hp0dHDTA6KrW+ibhgNb/J7vb85H/DXMq9OwdeVzeVXNMmIbPHfcajguxMxG9sbCzFrSa8FjSZcRsBpoDoeOxXf6fCPB9gwx/hXddZk5W+FFxPM/bj5N/GbPXoOiuv80WJH858Qfvqz/jXB+DPQzi4+IcPkKmKysjILsUjZ2lz4wWyA7JEetDXXqu38cehjH8d5s5OznM7j5DE2Hwcdd8KLQ315dHr17/YsGpRw7vGtpV3ZY1JZx4CJW4ssQxK4oOqPt25gRAD9COJktXnjvQRwziDZNGbM0zamdYnMGWsN8WU93u07qT8VMHohxI/nPiD99Wf8a+cS1cViY8ZSt3csDXqthY+B5Je1uhzPPm7p3Vaytg312zDIZzkc2Rw9870wAu8vn0WmJgmCvUtktEiFaf5ocSf5z4g/fVn/Gte464Dq8KYBmUoZXOC1DepMAnys8rC19qJjg5jnEEFrnDqPNbrR4Qq+y5oo72QdHbEb+d83vtA6jR1079Vr/pEwkeC9H9iGOexYa/JY95dYk5yP5bANA/DogJVYX5OJuHK5eKC5ViAyd3lilrOLtesy+fON9j1A8ispL9zHNL7sEctcdXWKnMeQfF0Z66+bSdfBXeddjfZNljfE9pG9a5A8w+Jz+0JeUx/leF/tb699dNqPvXUdx1XQuy+LvxehVYWZTRdkkGWugDXUaEbOHA8VxDtZhTMKvGuDswqgviILZJ5cOR8xzo5cHNPkDkILGPEjjzRzHHNfIG66fjOcE9PP4K4rtlZAxs0jZpQPee1nIHH5DZ1+tQsE0RVZ4GjUcFmaKNv+y0O2B9g3r7FYuc1jS5xDWtGySegC9nTa0DMOK8TVe4nIdY20Ch070tm3chkpy12QPDWTP8AozDzLfkO3Xz+WiZqqrPFeFqQPmlytNsbGeI4iZp93W96B2eisopWTxMkje2SN7Q5r2nYcCNgg/BTa4HSZKjUY4e8WwPFQIsVJHm5r5uSujkjDBXP0R2+etDRI6b252yRoCyRFUNDdlBzi+JRERSUURERERERERERERERFiZGNkawvaHu2WtJ6nXfQ89I7m5Ty65tdN9t+ShW8JWu5GrdlDzNX+hp3unWyNj5E76a+ex0U9RE6ypHKAIK16hhchjZZJK8eKifJ9Nw9YO+u/NxWwqFiRfFU+0TAbPO7/V98vL+T389d/n26KaoUmBjdFdrPc93vGeq2f0UBhyVkSOLWHNO5nNGyB7Hm2dbG/1hbY2thND/AJzudvzBv/GWq+iSu+3l5oIgHSS5ssaCdbJw8oC6Vj/RJm7UpZYZXpMDd+I54k2enTTTv/8AFxnFf92r/wCj9V2q3pVKthaZGZvcHPkOqz4cm4WivY6Ns+Yfb8aMAgsZE5/MNbbzHQ35bK2/jX02cI+j7MNxecyElW4YWzhja8kg5CSAdtBH5JUfF+jM0LdSZ1bGudC9jzI11gOJBB2B4mt9Phpb3Pjqtp/PNWhmfrXNJGHHX6QsW2NBr5uGlzeQIB8yD9Fu6VC9bQLaJax0jdpIj5S3XxVM/jzDMxuOvmy71bIQNsV3eE73mEAg6106Ed1nNxtiIGlz7DgBCyf/AEbvoPIDT2+YX3PVstuu3ExUfCa0hwst7dtBuvJQG1+JzUl548WbHMwM008vL13v/wBOv0qy6JOXZb9uYNGbfipEnpCwkUcT3WnBsoJb+Kd1AJB8viFrfpF4locQcD2vUpjL4WQxvPthbrd2HXf7Fjx2/inHYRtuG9jsRDXdA+e0Kwn5BzASbjI95mz1AIdreiDpU97iezxb6HTeu04Kd5mWp15xWcDHI5l+Ec7Pyg1w0QHgO0RseZAcVQu1yr88wwRtyGXkDGiR2Tu8ztdT/KpV7nsV5x/65lf/ADO9/wDalU3B8PP4w4no4QZCfGRTQz2JJ6zWmQiMN00c2wAS/qe/TQ7rstGtTtLBlV+jQ0fQL5zuberfYtVoU9XOe6J6E/sFTYb6N7/xs3/yWNqtdjbJL7UeyNpL+VtNjyG/ADuf7yrjibgCfhHhXLZ2LiHI3JMfmX0RWtNjMcsQlazrpoO9E9R5rycOVxHwOlOzuaV7SzUiYHgrWIWVfDa2SuBJ+R9VrOLy8McNLFZCezk7Vt0jPGmx742PA27ldtoGg3ps99de62UdAvqLPY0tEEz9+K1dR4eZAj18AIRERXFaRERERERERERERERERERERERERfaNPJ5m7YrYrD3crJWYySY1vDDWB5cGgl729TyO7fBfFGbXkrZN16s+MTPiEMkdhj3xPaCS08rJGEOHM7rvsda7LCvDcCiTaxn6rZYcLM3AF+SKesxv05rdeCMHxLgTelmwHEONue0o71SxRZTlc3VXwHBwkkLeoc/pr4Ha3f8ACjjf67jL93Yn71x/2lkPq8V+yWf4pPaWQ+rxX7JZ/ilzytguI16jqr2iSZ3Hquv23abBrWiyhTecrQANDsPBdg/Cjjcf9Nxl+7sT960jKf5STsJk7eOv8R8WVLtWV0M0MmKxoLXDuO3X7R0K1b2lkPq8V+yWf4pYPt25Hcz62Ge74uozk/22kpYDdNdNWnI6OA/Y/RVr9q8Pc2KFbKerHO/QFv18Fsf/ACpq3/evin91437ltHC/pW4h40xr7+FyPGN2m2V0Jmbi8W1pe0AkDet9x1HT9S5l6xZ/NMJ+wT/xK9G5C+xoa2LEtaOwbUsAD/3SuVcCrubFKkQer2n9Mo+qs0O1Vm10164cOlNzT5lzvouw/hRxv9dxl+7sT96rXuz09f1SejxXJQfbjuzVY8fi4WzSMkbIC4sId1cxpJB2fiuY+0sh9Xiv2Sz/ABSe0sh9Xiv2Sz/FLF/D9/8AlHmPVZ34twn858j6KVHwbxd6xdd+CGV/H3bM7Pfr/Rkne9u/x3fTgno74ko4zjHA5m7J6njpoJ6zpp9NbC6VreQyHs0czOUnegXDrrqoU17IzRPjBx0BcCPEhqzh7fm0myQD8Ng/Yo9KjDQoQ04mkwRRiJoeeYloGupPffn8V662tb2vbPtbyGtgARvp4rn17f4Za3lO/wAOzOfmLnToNeA0HM810308zY2HgZ2Kpy1/X8rkYpmV4pA58jjIHyzEAn3QASXduw7kBc6cduJ+J2o1TG08eXGrUgrF/wBIwxNZv7dBSVscMsDh9I0y7MSVpsaxUYtcCsGZQBG8+iIiLbrz6IiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIiIi//Z"
      }
    ];
    wrapper.setState({ courseList: mockCourses });
    expect(wrapper.find(CourseContainer).length).toBe(mockCourses.length);
  });
});
