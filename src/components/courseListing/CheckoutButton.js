import React, { Component } from "react";
import StripeCheckout from "react-stripe-checkout";
import axios from "axios";

class CheckoutButton extends Component {
  onCheckout = async checkoutToken => {
    const courseId = this.props.info._id;
    const userToken = localStorage.getItem("token");
    const res = await axios.post("http://localhost:8000/api/charge_money",{checkoutToken,courseId,userToken});
    console.log(res);
  };

  render() {
    const { topic, tuition, display, img } = this.props.info;

    return (
      <StripeCheckout
        stripeKey="pk_test_IQqSzL2RDKzgQnK2dJUc4kvo00b2cmxd73"
        amount={tuition * 100}
        image={img}
        currency="THB"
        token={this.onCheckout}
        name={"Buy " + topic + " course"}
        description={"Instruction by " + display }
        ComponentClass="form-group"
      >
        <button className="btn btn-secondary">Checkout</button>
      </StripeCheckout>
    );
  }
}

export default CheckoutButton;
