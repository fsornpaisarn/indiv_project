import React, { Component } from 'react';

export default class Footer extends Component {
  render() {
    return (
      <div className="footer fixed-bottom bg-dark">
        <br />
        <div className="copyR">Copyright © 2019 | Made by THRIVE </div>
        <br />
      </div>
    );
  }
}
